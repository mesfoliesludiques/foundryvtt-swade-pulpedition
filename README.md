# SWADE Pulp Edition module for Foundry VTT
This module Bennies and Vitals counters to bring a Pulp feel to the SWADE system on Foundry VTT.
Works only with the community sheet.
This module also adds an option to add diagonal movement following the rune 1/2/1 (As in DnD)

# Screenshots

![](counters.png)
![](diagonals.png)


## Setup
To install this module, go to the World Configuration and Setup, Addon Modules, Install Module and find it in the module Browser.
You can also add the following manifest https://gitlab.com/mesfoliesludiques/foundryvtt-swade-pulpedition/-/raw/master/module.json

## Contributions
Every contribution is welcome.

## Credits
Counter icons have been crafted by qdwag (Mark).