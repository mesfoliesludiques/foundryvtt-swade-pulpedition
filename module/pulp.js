/*



*/

Hooks.once('init', () => {
  game.settings.register("swade-pulpedition", "enableBennies", {
    name: "Enable the Benny Stack",
    hint: "Shows a benny stack instead of the default Benny counter.",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
  });
  
  
  game.settings.register("swade-pulpedition", "enableVitals", {
    name: "Enable the vitals counters",
    hint: "Shows an alternate version of wounds and fatigue counters.",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
  });

  game.settings.register("swade-pulpedition", "diagonalMovement", {
    name: "Diagonals movement",
    hint: "Adds 1 inch of movement for two diagonals used",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
  });

})

Handlebars.registerHelper("counter-bar", function (active, mult, value, max) {
  if (active) {
    return mult * value;
  }
  return mult * (max - value);
});

Handlebars.registerHelper("min", function (lh, rh) {
  return lh < rh ? lh : rh;
});

Handlebars.registerHelper("incapacited", function (fatigue, wounds) {
  if (fatigue.value > fatigue.max || wounds.value > wounds.max)
    return true;
  return false;
});

function _replaceBennies(sheet, html, entity) {
  // Bennies
  html.find(".bennies").remove();

  let bennyStack = `<div class="bennies"><div class="benny-value">${entity.data.bennies.value}</div><div class="benny-stack"></div></div>`;
  html.find(".sheet-header").append(bennyStack);

  // Stack bennies to match the actor values
  let stack = html.find(".benny-stack");
  stack.append('<div class="benny-chip"></div>');
  for (let i = 0; i < entity.data.bennies.value; ++i) {
    stack.append('<div class="benny-chip active"></div>');
  }

  // Event listener
  // Bennies
  html.find(".benny-chip").click((event) => {
    event.preventDefault();
    let el = event.currentTarget;
    if (el.classList.contains("active")) {
      sheet.actor.spendBenny();
    } else {
      sheet.actor.getBenny();
    }
  });
}

async function _replaceVitals(sheet, html, entity) {
  // Counters
  html.find(".vitals-container").remove();

  let counter_data = {
    wounds: {
      value: entity.data.wounds.value,
      max: entity.data.wounds.max,
    },
    fatigue: {
      value: entity.data.fatigue.value,
      max: entity.data.fatigue.max,
    },
  };
  const counters = await renderTemplate(
    "/modules/swade-pulpedition/templates/counters.html",
    counter_data
  );
  html.find(".vitals").prepend(counters);

  // Event listeners
  html.find(".counterbar").click((event) => {
    event.preventDefault();
    let el = event.currentTarget;
    if (el.parentElement.parentElement.classList.contains("fatigue")) {
      if (el.dataset.value < 2) {
        sheet.actor.update({
          "data.fatigue.value": parseInt(el.dataset.value) == entity.data.fatigue.value ? entity.data.fatigue.value - 1 : parseInt(el.dataset.value),
        });  
      } else if (entity.data.fatigue.value >= 2) {
        sheet.actor.update({
          "data.fatigue.value": entity.data.fatigue.value < entity.data.fatigue.max ? entity.data.fatigue.value + 1 : 1,
        });  
      } else {
        sheet.actor.update({
          "data.fatigue.value": 2,
        });
      }
    } else if (el.parentElement.parentElement.classList.contains("wounds")) {
      if (el.dataset.value < 3) {
        sheet.actor.update({
          "data.wounds.value": parseInt(el.dataset.value) == entity.data.wounds.value ? entity.data.wounds.value - 1 : parseInt(el.dataset.value),
        });  
      } else if (entity.data.wounds.value >= 3) {
        sheet.actor.update({
          "data.wounds.value": entity.data.wounds.value < entity.data.wounds.max ? entity.data.wounds.value + 1 : 2,
        });  
      } else {
        sheet.actor.update({
          "data.wounds.value": 3,
        });
      }
    } else if (el.parentElement.classList.contains('incapacited')) {
      if (entity.data.wounds.value == entity.data.wounds.max) {
        sheet.actor.update({
          "data.wounds.value": parseInt(entity.data.wounds.value) + 1,
        });
      }
      else if (entity.data.fatigue.value == entity.data.fatigue.max) {
        sheet.actor.update({
          "data.fatigue.value": parseInt(entity.data.fatigue.value) + 1,
        });
      } else if (entity.data.wounds.value > entity.data.wounds.max) {
        sheet.actor.update({
          "data.wounds.value": parseInt(entity.data.wounds.value) - 1,
        });
      } else if (entity.data.fatigue.value > entity.data.fatigue.max) {
        sheet.actor.update({
          "data.fatigue.value": parseInt(entity.data.fatigue.value) - 1,
        });
      }
    }
  });
}

Hooks.on("renderSwadeCharacterSheet", (sheet, html, entity) => {
  if (game.settings.get('swade-pulpedition', 'enableBennies')) {
    _replaceBennies(sheet, html, entity);
  }
  if (game.settings.get('swade-pulpedition', 'enableVitals')) {
    _replaceVitals(sheet, html, entity);
  }
});

export const measureDistances = function(segments, options={}) {
  if ( !options.gridSpaces ) return BaseGrid.prototype.measureDistances.call(this, segments, options);

  // Track the total number of diagonals
  let nDiagonal = 0;
  const rule = this.parent.diagonalRule;
  const d = canvas.dimensions;

  // Iterate over measured segments
  return segments.map(s => {
    let r = s.ray;

    // Determine the total distance traveled
    let nx = Math.abs(Math.ceil(r.dx / d.size));
    let ny = Math.abs(Math.ceil(r.dy / d.size));

    // Determine the number of straight and diagonal moves
    let nd = Math.min(nx, ny);
    let ns = Math.abs(ny - nx);
    nDiagonal += nd;

    // Alternative DMG Movement
    if (rule) {
      let nd10 = Math.floor(nDiagonal / 2) - Math.floor((nDiagonal - nd) / 2);
      let spaces = (nd10 * 2) + (nd - nd10) + ns;
      return spaces * canvas.dimensions.distance;
    }

    // Euclidean Measurement
    else if (rule === "EUCL") {
      return Math.round(Math.hypot(nx, ny) * canvas.scene.data.gridDistance);
    }

    // Standard PHB Movement
    else return (ns + nd) * canvas.scene.data.gridDistance;
  });
};


Hooks.on("canvasInit", () => {
  // Extend Diagonal Measurement
  canvas.grid.diagonalRule = game.settings.get("swade-pulpedition", "diagonalMovement");
  SquareGrid.prototype.measureDistances = measureDistances;
})
